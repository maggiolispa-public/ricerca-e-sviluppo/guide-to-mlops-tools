# Guide to MLOps tools

As R&D we have experimented with some MLOps tools, useful for testing and deploying machine learning models.
Below are instructions on how to install and use them.

## DVC

[Homepage](https://dvc.org)

**DVC** is a tool that makes ML models shareable and reproducible.
It can manage huge files, datasets, ML models, metrics and source codes.
We used it for data versioning.

This tool works very similar to [**GIT**](https://git-scm.com/): you can push and pull data to and from a remote repository, keeping track of its version via a file generated and stored in the project root folder.
It can be installed by typing:

```bash
pip install dvc
```

As a first step we need to configure the [remote repository](https://dvc.org/doc/command-reference/remote) where data will be saved.
The repository can be a local folder, a network shared folder or a cloud repository (like [**Google Cloud Storage**](https://cloud.google.com/storage), [**Google Drive**](https://www.google.com/intl/it/drive/terms-of-service/), [**AWS S3**](https://aws.amazon.com/it/s3/), etc.).
For instance, we can type the below command to configure a local folder as repository:

```bash
dvc remote add -d myremote "/path/to/local/folder"
```

To configure **Google Drive** as repository, we can use an **URL** in the format:

```
gdrive://<base>/path/to/folder
```

where `<base>` can be one of the following:

* the **UID** of the **Google Drive** folder
* `root` that indicates your topmost **Google Drive** folder ("**My Drive**")

A configuration file will automatically be generated, which can be archived with **GIT**:

```bash
git add .dvc/config
git commit -m "Configure remote storage"
```

Once new data are added, then need [to be added](https://dvc.org/doc/command-reference/add) to **DVC** control by typing:

```bash
dvc add "/path/to/dataset/folder"
```

**DVC** will automatically create a small `.dvc` file with information about the current version of the data.
This file must be archived with **GIT**:

```bash
git add "/path/to/dataset/folder/.dvc"
git commit "/path/to/dataset/folder/.dvc" -m "Changed dataset"
```

Now the data can be pushed to the remote repository and finally the changes can [be pushed](https://dvc.org/doc/command-reference/push) to **GIT**:

```bash
dvc push
git push
```

If you want [to pull](https://dvc.org/doc/command-reference/pull) the updated version of the data, the command is as follows:

```bash
dvc pull
```

Finally, if you are looking for a [specific version](https://dvc.org/doc/command-reference/checkout) of the data, you need to search for that version of the `.dvc` file (using **GIT**), then you can type:

```bash
dvc checkout
```

## MLFlow

[Homepage](https://mlflow.org/)

**MLFlow** is an open source platform that manages the ML model life-cycle end-to-end.

It can be installed by typing:

```bash
pip install mlflow
```

**MLFlow** provides a **GUI** that helps to manage your experiments, the models and the results.
If you use [**SQLite**](https://www.sqlite.org/index.html) to log data, to launch the **GUI** you can use this command:

```bash
mlflow ui --backend-store-uri sqlite:///path/to/database/file -h <host_addess> -p <port>
```

If you log data in a local folder, you can use:

```bash
mlflow ui -h <host_addess> -p <port>
```

Finally you can open the browser using the **URL** `http://<host_addess>:<port>`

There are four main components:

1. **MLFlow Tracking**: to track experiments, log and compare parameters and results
2. **MLFlow Projects**: to package code into a reusable and reproducible form
3. **MLFlow Models**: to deploy models and to run them on a variety of inference platforms
4. **MLFlow Model Registry**: to manage the ML Model life-cycle, versioning, stage transitions and annotations

### MLFlow Tracking

[Homepage](https://mlflow.org/docs/latest/tracking.html)

This component tracks all features of your experiment, like parameters, metrics, results, artifacts.
All these data can be saved locally (with or without **SQLite**) or remotely.
For instance, you can set the tracking locally with **SQLite** with the following command:

```python
mlflow.set_tracking_uri("sqlite:///path/to/database/file")
```

Next you need to create the experiment where everything will be tracked.
**MLFlow** manages experiments divided into **runs**, i.e. individual tests.
You can create a new experiment (or open an existing one) and create a new **run**:

```python
try:
    exp_id = mlflow.create_experimenti(name="experiment_name")
except Exception as e:
    exp_id = mlflow.get_experiment_by_name("experiment_name").experiment_id

mlflow.start_run(
    experiment_id=exp_id,
    run_name="the_name_of_the_new_run",
    tags={"dictionary": "of", "custom": "tags"},
    description="Description of the new run"
)
```

To track parameters or metrics or artifacts, you can use the following commands:

```python
mlflow.log_param("param_name", param_value)
mlflow.log_metric("metric_name", metric_value, step=epoch)
mlflow.log_artifact("/path/to/local/artifact/file_or_dir", artifact_path="/path/to/artifact/")
```

To create a package of the model to reuse it within **MLFlow**, you can use the following code:

```python
mlflow.pytorch.log_model(model, "model_name")
```

**MLFlow** can use a variety of model types even using dedicated [plug-ins](https://mlflow.org/docs/latest/plugins.html).
For instance, to use a [**PyTorch**](https://pytorch.org/) model, you can use the [**MLFlow.pytorch**](https://mlflow.org/docs/latest/python_api/mlflow.pytorch.html) plug-in.
Finally you can stop the experiment run with the following command:

```python
mlflow.end_run()
```

In the **GUI** you can manage experiments and **runs** in the **Experiments** page.

![](./images/GUI01.png)

In the side menu, you can select the experiment to show, while the **runs** are shown in the table.
With buttons you can filter the **runs** according with the selected criteria.

Selecting a **run**, the details are shown in a new page:

![](./images/GUI02.png)

All parameter values used for this run, the metrics obtained, and the artifacts produced are listed here.

By clicking on a metric you can see its progress during the training epochs, thanks to a graph. 

![](./images/GUI03.png)

Finally, by selecting two or more runs, you can compare results by clicking on **Compare** button.

![](./images/GUI04.png)

### MLFlow Projects

[Homepage](https://mlflow.org/docs/latest/projects.html)

This component manages a project to make it easier for other data scientists to rerun an experiment.
This is all done by creating an `MLproject` file, structured in three parts:

1. The name of the project
2. The environment in which to perform the experiment.
For instance, the [**Conda**](https://docs.conda.io/en/latest/) environment with the `conda.yaml` file that contains all the project dependencies.
But other environments are supported, such as [**Docker**](https://www.docker.com/).
3. The entrypoints, i.e. commands that will be executed to run the experiment.
For instance a first entrypoint to run the training phase of the model and a second entrypoint to run the test phase of the model.
An entrypoint can include some parameters (with their default value) to change the behavior of the model.

To execute a specific entrypoint of an experiment, you can use the command below:

```bash
mlflow run . --entry-point "entrypoint-name" --experiment-name="example experiment" -P param1=param1_value -P param2=param2_value ...
```

Any parameter omitted will use its default value as defined in the `MLproject` file. 
This command will create a new environment (**Conda** or **Docker** or anything else defined in `MLproject` file) with the necessary libraries and dependencies and then run the experiment.

### MLFlow Models

[Homepage](https://mlflow.org/docs/latest/models.html)

This component defines a standard format for packaging ML models so they can be used in a variety of tools or platforms (such as inference services).
It provides several ways (or "**flavors**") to save the model, depending on its nature.
Flavors are the key concept that makes **MLFlow Models** powerful: they are a convention that deployment tools can use to understand the model, which makes it possible to write tools that work with models from any ML library without having to integrate each tool with each library.

Every flavor that a model can support are defined in a `MLmodel` file in [**YAML**](https://yaml.org/) format.
By saving a model you can provide its [**signature**](https://mlflow.org/docs/latest/models.html#model-signature-and-input-example), i.e. its input and output data schemas. 
For instance:

```python
input_schema = Schema([TensorSpec(numpy.dtype(numpy.float32), (1, 13, 30, 48, 48))])
output_schema = Schema([TensorSpec(numpy.dtype(numpy.float32), (2, 18, 48, 48))])

signature = ModelSignature(inputs=input_schema, outputs=output_schema)
```

If a signature is provided, every time the data is applied to the model, it will check if their structure is equal to the schema in the signature, throwing an exception in case of incompatibility.
To save the model (for instance a **PyTorch** model), you can use the below command:

```python
mlflow.pytorch.log_model(my_model, "name of the model", signature=signature)
```

This command create all necessary files, included the `MLmodel` file.
Once saved, you can load it using this command:

```python
model = mlflow.pytorch.load_model("URI of the model")
```

At this point you can apply the data to the model:

```python
y = model(x)
```

This is the method used by all **MLFlow Models** flavors.
By logging a model it is possible to manage, through the **GUI**, its life-cycle in a collaborative way, check all versions, stage transitions and annotations.
To log the model you need to select the **run** that produced it as an artifact.
By selecting the folder containing all the model files produced by `log_model` function, simply click on the **Register Model** button, assign it a name and save it.

![](./images/GUI05.png)

By clicking the **Models** section, a page with the details of the registered models opens.
For each model you can see its version (that is updated every time a new model with the same name is logged) and its stage.

![](./images/GUI06.png)

Clicking on a model opens a page where it is possible to change the stage of the model.
For instance it is possible to change the stage from **Staging** to **Production**.
This way it is possible to control the models life-cycle and know which model is in production stage.

![](./images/GUI07.png)

All these steps are also provided as **APIs**.
For instance, you can use the below code:

```Python
model = mlflow.pytorch.load_model(model_uri=f"models:/{model_name}/{stage}")
```

Documentation is available at [this link](https://mlflow.org/docs/2.1.1/index.html).

## MLFlow-TorchServe Plug-In

[Homepage](https://github.com/mlflow/mlflow-torchserve)

[**TorchServe**](https://pytorch.org/serve/) is a performant and flexible tool for serving **PyTorch** models and make them available for data prediction.
It can be used with **MLFlow** thanks to the **MLFlow-TorchServe** plug-in.
The **MLFlow-TorchServe** plug-in can be installed as follow:

```bash
pip install mlflow-torchserve
```

As first step, you have to run **TorchServe** by typing:

```bash
torchserve --start --ts-config /path/to/the/config/file --model-store /path/to/the/models/folder
```

where 

* `--model-store` is a mandatory parameter indicating the folder where local models are stored.
From this folder **TorchServe** retrieves the model to serve
* `--ts-config` is an optional parameter indicating the configuration file.
In this file you can define a variety of parameters, such as the inference URL and the size limit for a request

To stop **TorchServe** you can type:

```bash
torchserve --stop
```

### Deployment

Once started **TorchServe** you need [to create a deployment](https://github.com/mlflow/mlflow-torchserve#create-deployment) for the model created with **MLFlow**.
This part can be handled by a code like the following:

```python
from mlflow.deployments import get_deploy_client

target_id = "torchserve"
plugin = get_deploy_client(target_id)
plugin.create_deployment(
    name="deployment_name", 
    model_uri="URI_to_model", 
    config={
        "MODEL_FILE": "/path/to/model/file", 
        "HANDLER": "/path/to/handler/file",
        "EXTRA_FILES": ["/path/to/my/extra/files"],
        "EXPORT_PATH": "/path/to/export/folder"
    }
)
```

where:

* `target_id` is the deployment target identification (in this case `torchserve`)
* `MODEL_FILE` is the [**Python**](https://www.python.org/) file with the code that manages the machine learning model
* `HANDLER` is the **Python** file with the code that manages the inference requests
* `EXTRA_FILES` is a list of any additional files to send to **TorchServe** and which are used for the correct functioning of the model.
For instance, configuration files or **Python** files to provide some functionality (like sending e-mail, saving data, drawing graphs, etc.)
* `EXPORT_PATH` is the path to the folder where all necessary files will be saved and then used by **TorchServe**.
This path must match the `model_store` parameter of the previous command. 
* `name` is the name of the deployment
* `model_uri` is the **URI** to the weights dictionary of the trained model to use.
It can be a local path or a remote path (like an **AWS S3** repository). See [there](https://www.mlflow.org/docs/latest/concepts.html#artifact-locations) for more information.

The result of creating a deployment is a new folder (see `EXPORT_PATH`) with a file with `.mar` extension which contains all files needed.
This file will be used by **TorchServe** to serve the model.

All files within `.mar` file are accessible as below:

```python
properties = context.system_properties
model_dir = properties.get("model_dir")
path_to_my_file = os.path.join(model_dir, "my_file.ext")
```

[To delete a deployment](https://github.com/mlflow/mlflow-torchserve#delete-deployment) you can use a code like as follow:

```python
plugin.delete_deployment(name="deployment_name")
```

The handler file manages the model during deployment creation and inference.
It contains a class inherited from the **TorchServe** [`BaseHandler`](https://pytorch.org/serve/api/ts.torch_handler.html#module-ts.torch_handler.base_handler) (or [its derived](https://pytorch.org/serve/README.html#default-handlers)).
This class must overwrite these methods:
* `initialize`: this method is called during deployment creation and handles initialization of the model, such as creating a new instance, loading weights, and so on. 
* `preprocess`: this method handles data preprocessing to fit the input data for the model (like [**JSON**](https://www.json.org/) deserializing, normalizing, resizing, etc.) 
* `postprocess`: this method handles data postprocessing to convert the model prediction into more usable information (like **JSON** serializing, encoding, etc.)
* `handle`: this method handles the inference phase (like data processing, running the prediction, returning the result, etc.)

### Predictions

Once deployment is created, the model is ready to receive data and provide predictions.
[To get prediction](https://github.com/mlflow/mlflow-torchserve#run-prediction-on-deployed-model), the code is as follow:

```python
plugin = get_deploy_client("torchserve")
pred = plugin.predict("deployment_name", input_data) 
```

The `input_data` can be a [**Pandas**](https://pandas.pydata.org/) dataframe, a [**tensor**](https://pytorch.org/docs/stable/tensors.html) or a **JSON** string.

**TorchServe** automatically creates a `log` folder where [it tracks](https://pytorch.org/serve/logging.html) all the accesses, handler messages, model metrics (such as prediction time), **TorchServe** messages and performances, etc.

## AirFlow

[Homepage](https://airflow.apache.org/)

**Apache AirFlow** is an open source platform for developing, scheduling and monitoring of workflows.
Can create pipelines and manages them through a web interface.
It can be installed in several ways, such as [**Docker**](https://www.docker.com/) container:

```bash
docker pull apache/airflow:latest-python3.8
```

**AirFlow** provides a [`docker-compose.yaml`](https://airflow.apache.org/docs/apache-airflow/2.5.1/docker-compose.yaml) file to manage its components:

```bash
curl -Lf0 'https://airflow.apache.org/docs/apache-airflow/<version>/docker-compose.yaml' > dockercompose.yaml
```

where `<version>` is the **AirFlow** version, for example `2.4.2` or `2.5.1` etc. 

You can customize it by changing some parts:

1. The **Docker** image it starts from can be changed by setting the `AIRFLOW_IMAGE_NAME` variable (default `apache/airflow:<version>`)
2. The executor, i.e. the mechanism by which tasks get run, can be changed by setting the `AIRFLOW__CORE__EXECUTOR` variable (default `CeleryExecutor`, see [there](https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/executor/))
3. Setting the `AIRFLOW__CORE__LOAD_EXAMPLES` variable to `false` the pipeline examples are not loaded
4. There are some **Docker** volumes that can be customized
5. In case you need [to use the GPU](https://docs.docker.com/compose/gpu-support/), you can add a `deploy` section like the below:
```yaml
  deploy:
      resources:
        reservations:
          devices:
            - driver: nvidia
              count: all
              capabilities: [gpu]
```
6. To avoid memory saturation during training, you can add [`shm_size`](https://docs.docker.com/compose/compose-file/compose-file-v3/#shm_size) section like below:
```yaml
  shm_size: '2gb'
```

Before starting **AirFlow** for the first time, you need to prepare the environment by creating necessary files and folders.
The **AirFlow** environment needs three folders:

1. The `dags` folder where to save the pipeline source code
2. The `logs` folder
3. The `plugins` folder

If you are using **Linux** you need to run [the below command](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html#setting-the-right-airflow-user):

```bash
echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env
```

This ensures that folders and files created by **AirFlow** inside the **Docker** container will have access granted to the host user.
Next, for the first time only, you need to initialize the database and create the user account for the **AirFlow** web **GUI**.
The default credentials are `user=airflow` and `password=airflow`, but you can change them by setting the variable `_AIRFLOW_WWW_USER_USERNAME` and `_AIRFLOW_WWW_USER_PASSWORD` inside the `docker-compose.yaml` file.
[To initialize](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html#initialize-the-database) the **AirFlow** environment, use this command:

```bash
docker-compose up airflow-init
```

Once initialized, you can [run](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html#initialize-the-database) the necessary **Docker** containers by typing:

```bash
docker-compose up -d
```

Four containers will be created, one per process:

1. [**Scheduler**](https://airflow.apache.org/docs/apache-airflow/stable/administration-and-deployment/scheduler.html) that monitors tasks and pipelines, executing tasks when all dependencies are completed.
It's designed to run as a persistent service in an **AirFlow** production environment 
2. [**Triggerer**](https://airflow.apache.org/docs/apache-airflow/stable/authoring-and-scheduling/deferring.html) that performs tasks after events occur
3. [**Webserver**](https://airflow.apache.org/docs/apache-airflow/stable/administration-and-deployment/security/webserver.html) that provides a **GUI** and a pipelines manager
4. [**Postgres**](https://airflow.apache.org/docs/apache-airflow/stable/howto/set-up-database.html) the database where pipelines run data are stored

To remove the created containers, you have to use this command:

```bash
docker-compose down -v
```

### AirFlow DAG

[Homepage](https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/dags.html)

**AirFlow** manages pipelines using **Python** scripts in the `dags` folder.
A **dag** or **pipeline** is a sequence of tasks that performs a job and returns a result.
To declare a **dag**, the code is as follow:

```python
with DAG(
    dag_id="my_dag_id",
    default_args=default_args,
    start_date=datetime(2022, 12, 28, 13, 54),
    schedule_interval="@monthly"
) as dag:
    # some code
    pass
```

where:
* `default_args` is a dictionary with general information about the **dag**, like user information, retries after failure, etc.
* `schedule_interval` is an optional parameter that indicates how often the **dag** must be executed.

There are several types of task (or **operator**) you can use, below are the four types we experimented with:

1. [**EmptyOperator**](https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/operators/empty/index.html), as the name says, is a dummy operator that does nothing, but is represented graphically, so it's normally used as a *start* and *stop* task
2. [**BashOperator**](https://airflow.apache.org/docs/apache-airflow/stable/howto/operator/bash.html) is an operator that runs [**Bash Shell**](https://www.gnu.org/software/bash/) commands (**important note**: if you want to run a script, you must insert a space at the end of the command string)
3. [**PythonOperator**](https://airflow.apache.org/docs/apache-airflow/stable/howto/operator/python.html) is an operator that runs **Python** code, which can be defined as a function in the **dag** file or in an other file
4. [**BranchPythonOperator**](https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/operators/branch/index.html) is an operator that runs **Python** code and choose the next task to execute depending on the result

For the complete list of operators, see [this link](https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/operators/index.html).

Each task within the **dag** can depend on the result of its previous ones. 
These dependencies are made explicit using the operator [`>>`](https://docs.astronomer.io/learn/managing-dependencies#basic-dependencies).
For instance, if the second task depends on the first running successfully, the code would be as follow:

```
first_task >> second_task
```

This creates the pipeline dependency chain.

The tasks, even if isolated, can communicate with each other through the [**XCom**](https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/xcoms.html#xcoms) mechanism.
Each **XCom** is identified by a key (its name) and the `task_id` and `dag_id` it comes from.
**XCom** can have any value, but they are designed for small amounts of data.
If the `do_xcom_push` parameter is set to `True` (as is by default), all variables returned by functions performed by tasks are saved as **XCom**.
Otherwise the variables must be saved manually with the `xcom_push` function.
Finally, to fetch data previously saved in a **Python** function, the code to use is as follow:

```python
def get_value(task_instance): 
    return task_instance.xcom_pull(task_ids="value_pushing_task")
```

### AirFlow UI

[Homepage](https://airflow.apache.org/docs/apache-airflow/stable/ui.html)

Once containers and pipeline are created, you can open the web **GUI** using the **URL** `http://<dockerurl>:<port>`, where `<port>` is defined in the `docker-compose.yaml` file (default is `8080`).
To login you must use the credentials defined in the same configuration file.
The homepage shows the list of pipelines created, as follow:

![](./images/GUI08.png)

From this page it is possible to pause or activate the pipeline scheduling.
It is also possible to start a pipeline immediately by pressing the **play** button. 
Selecting a pipeline opens the manager page: it contains all information on previous executions.
You can view the pipeline tasks graph by clicking on **Graph** button, in order to check if the tasks dependencies defined in the **dag** file are correct.
Furthermore, running the **dag**, the graph will show the running task in real-time, the results and the ignored tasks.

![](./images/GUI09.png)

**AirFlow** provides the ability to view the logs of each individual task to understand its behavior and the reason for any failures. 
To do this, just click on a single task (that has been performed at least once) and select the log entry. 
Furthermore, from this page you can also manage the behavior of success or failure manually:

![](./images/GUI10.png)

### AirFlow REST API

[Homepage](https://airflow.apache.org/docs/apache-airflow/stable/stable-rest-api-ref.html)

**DAGs** can be controlled and activated even from **REST APIs**.
For instance, you can activate a pipeline as follow:

```python
url = 'http://<airflow_address>:<port>/api/v1/dags/my_dag_name?update_mask=is_paused'
resp = requests.patch(
    url,
    headers={'Content-Type': 'application/json'},
    data='{"is_paused": false}',
    auth=('airflow', 'airflow')
)
```

and then run it:

```python
url = 'http://<airflow_address>:<port>/api/v1/dags/my_dag_name/dagRuns'
resp = requests.post(
    url,
    headers={'Content-Type': 'application/json'},
    data='{"dag_run_id": "my_dag_run_id"}',
    auth=('airflow','airflow')
)
```

## General workflow

In general, to manage a complete pipeline of **MLOps** with the tools we have presented, it is possible to proceed as follows:

1. Initialize the project folder using **DVC** and **GIT**
2. Use **MLFlow** to keep track of the experiments in order to find the best model
3. Use **AirFlow** to define a **dag** which manages the model life-cycle (testing it and deploying it if test metrics improves)
4. You can use several way to run the model, for instance **TorchServe** for models defined in **PyTorch**
5. The first time you have to manually run the **dag** in **AirFlow**. 
In this way the model will be tested and the result will be the new baseline.
Eventually the **dag** will deploy the model in production
6. At this point the model is ready to inference data
7. By monitoring the inference performance, through **MLFlow**, you can save inputs that are predicted wrong (for example if the confidence is below a threshold or if the result doesn't pass a cross check or whatever)
8. Once sufficient data has been collected, **MLFlow** will notify an operator (e.g. by sending an email) or start a preparation batch (if the data can be automatically labeled, e.g. if a user provide a feedback)
9. At this point the operator can prepare the data for a new training phase (for instance labeling them) or the preparation batch can automatically prepare the new dataset
10. Once the data is ready, the operator (or the batch) runs the **dag** which will test the model with the new dataset and possibly retrain it until performance improves (compared to the results earlier)
Finally the **dag** will deploy the updated model in production.

And the cycle will start again from step 6.
